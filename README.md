# Minikube environment
Creates minic=kube environment to demo purposes

# Install
```bash
./create_demo.sh
```

# Dependencies
* vagrant (https://www.vagrantup.com/intro/index.html)
* vagrant as 2018 05 25 requires virtualbox 4.0 - 4.3
